-- Change autoUpdate to false if you wish to not receive auto updates.
local version    = 0.004
local autoUpdate = true

--[[
 ___  _ ____ __ _ ____
 |__> | |--| | \| |--|

 The true power of the moon


 Requirements:
    - VPrediction

 Features:
    - Q Harass
    - E Interrupt
    - Smart KS without wasting R if there are still enemies in range
    - Auto ignite
    - Auto items
    - Auto farming (with spells or just aa last hit)
    - Orbwalking

 Changelog:
    - Version 0.001 (02/27/2014):
        - Initial release
    - Version 0.002 (03/01/2014):
        - Added orbwalker
        - Fixed auto-updater
    - Version 0.003 (03/02/2014):
        - Fixed spelling
        - Added raw combo
        - General improvements
    - Version 0.004 (03/03/3014):
        - Added UltManager
        - Added raw Item class
        - Fixed combo timing
        - General fixes and improvements

 Credits:
    - Honda7: Free scripts, not accpepting donations... dude, you are awesome!
              Wish there were more like you.
    - Ascii Generator: http://www.network-science.de/ascii/ (cybersmall)

]]

if myHero.charName ~= "Diana" or not VIP_USER then return end

-- Requirements
require "VPrediction"

-------------Updater-------------

local UPDATE_HOST      = "bitbucket.org"
local UPDATE_PATH      = "/Hellsing/botoflegends/raw/master/Diana.lua"
local UPDATE_FILE_PATH = SCRIPT_PATH..GetCurrentEnv().FILE_NAME
local UPDATE_URL       = "https://"..UPDATE_HOST..UPDATE_PATH

local webResult = nil
if autoUpdate then
    DelayAction(function() GetAsyncWebResult(UPDATE_HOST, UPDATE_PATH, function(data) webResult = data end) end, 3)
    function updateScript()
        if webResult then
            local serverVersion
            local startPos, endPos, junk = nil, nil, nil
            junk, startPos = string.find(webResult, "local version    = ")
            if startPos then
                endPos, junk = string.find(webResult, "\n", startPos)
            end
            if endPos then
                serverVersion = tonumber(string.sub(webResult, startPos, endPos))
            end
            if serverVersion and serverVersion > version then
                DownloadFile(UPDATE_URL.."?nocache"..myHero.charName..os.clock(), UPDATE_FILE_PATH, function () print("<font color=\"#8080F0\">"..player.charName..": Successfully updated. (v"..version.." -> v"..serverVersion..")</font>") end)
            elseif serverVersion and serverVersion < version then
                print("<font color=\"#8080F0\">"..player.charName..": Don't forget to upload the script :)</font>")
            elseif serverVersion then
                print("<font color=\"#8080F0\">"..player.charName..": You've got the latest version: v"..serverVersion.."</font>")
            else
                print("<font color=\"#8080F0\">"..player.charName..": Something went wrong! Please manually update the script!</font>")
            end
            webResult = nil
        end
    end
    AddTickCallback(updateScript)
end

------------ Globals ------------

local player = myHero
local target = nil

local ts          = nil
local menu        = nil
local VP          = nil
local ultManager = nil

local currentHealth = nil
local currentMana   = nil

local enemies = {}

local lastAttack     = 0
local lastWindUpTime = 0
local lastAttackCD   = 0

local spells = {[_Q] = { spellName = "DianaArc",      range = 925, level = 0, ready = false, manaUsage = 0, cooldown = 0, delay = 0.25, width = 195, speed = 1600, buffName = "dianamoonlight" },
                [_W] = { spellName = "DianaOrbs",     range = 200, level = 0, ready = false, manaUsage = 0, cooldown = 0, delay = 0.25 },
                [_E] = { spellName = "DianaVortex",   range = 450, level = 0, ready = false, manaUsage = 0, cooldown = 0, delay = 0.25, width = 450, speed = math.huge },
                [_R] = { spellName = "DianaTeleport", range = 825, level = 0, ready = false, manaUsage = 0, cooldown = 0, delay = 0.25 }}

local ignite = nil

----------- Constants -----------

local Q, W, E, R = _Q, _W, _E, _R

local colorRangeReady        = ARGB(255, 200, 0,   200)
local colorRangeComboReady   = ARGB(255, 255, 128, 0)
local colorRangeNotReady     = ARGB(255, 50,  50,  50)
local colorIndicatorReady    = ARGB(255, 0,   255, 0)
local colorIndicatorNotReady = ARGB(255, 255, 220, 0)
local colorInfo              = ARGB(255, 255, 50,  0)

local statsSequence = {Q, W, Q, E, Q, R, Q, W, Q, W, R, W, W, E, E, R, E, E}


--[[
  ______ _______ __   _ _______  ______ _______             _______ _______               ______  _______ _______ _     _ _______
 |  ____ |______ | \  | |______ |_____/ |_____| |           |       |_____| |      |      |_____] |_____| |       |____/  |______
 |_____| |______ |  \_| |______ |    \_ |     | |_____      |_____  |     | |_____ |_____ |_____] |     | |_____  |    \_ ______|
]]

function OnLoad()

    -- Setup enemy table
    for _, enemy in ipairs(GetEnemyHeroes()) do
        enemies[enemy] = { champ = enemy, name = enemy.charName, damageQ = 0, damageR = 0, ready = true}
    end

    -- Add tick callbacks
    AddTickCallback(updateVars)
    AddTickCallback(checkCombo)
    AddTickCallback(checkHarass)

    -- Check if player has ignite
    local igniteSlot = ((player:GetSpellData(SUMMONER_1).name:find("SummonerDot") and SUMMONER_1) or (player:GetSpellData(SUMMONER_2).name:find("SummonerDot") and SUMMONER_2) or nil)  
    if igniteSlot then
        ignite = {slot = igniteSlot, range = 600, ready = false}
    end

    -- Setup stuff
    setupVars()
    setupMenu()

end

function OnLevelUp(unit, level, remainingLevelPoints)

    -- Prevent errors
    if not unit or not unit.valid or not unit.isMe then return end

    if menu.utility.autoStats then

        local count = remainingLevelPoints
        local countTable = {}

        for i, spell in ipairs(statsSequence) do

            -- Only check till our level and only for the amount of points to assign
            if i > level or count == 0 then break end

            countTable[spell] = { value = countTable[spell] and countTable[spell].value + 1 or 1}

            if countTable[spell].value > player:GetSpellData(spell).level then
                -- Level spell since it needs to be highter than our level is atm
                LevelSpell(spell)
                count = getAssignedStats()
            end
        end
    end

end

function OnProcessSpell(unit, spell)

    -- Prevent errors
    if not unit or not unit.valid or not unit.isMe then return end
  
    if spell.name:lower():find("attack") then
        lastAttack = GetTickCount() - GetLatency() / 2
        lastWindUpTime = spell.windUpTime * 1000
        lastAttackCD = spell.animationTime * 1000
    end

    if unit.isMe and menu.utility.debug then
        PrintChat("Used spell: " .. spell.name)
    end

end

function OnGainBuff(unit, buff)

    -- Prevent errors
    if not unit or not buff or not unit.valid then return end

    -- Debug
    if menu.utility.debug then
        if buff.name:lower():find("diana") then
            PrintChat(unit.name.." gained buff: "..buff.name)
        end
    end

end


function OnUpdateBuff(unit, buff)

    -- Prevent errors
    if not unit or not buff or not unit.valid then return end

    -- Debug
    if menu.utility.debug then
        if buff.name:lower():find("diana") then
            PrintChat(unit.name.." updated buff: "..buff.name)
        end
    end

end


function OnLoseBuff(unit, buff)

    -- Prevent errors
    if not unit or not buff or not unit.valid then return end

    -- Debug
    if menu.utility.debug then
        if buff.name:lower():find("diana") then
            PrintChat(unit.name.." lost buff: "..buff.name)
        end
    end

end


--[[
 _______  _____  _______ ______   _____       _______ _______ _     _ _______ _______
 |       |     | |  |  | |_____] |     |      |______    |    |     | |______ |______
 |_____  |_____| |  |  | |_____] |_____|      ______|    |    |_____| |       |      
]]

function checkCombo()
    if menu.combo.active then

        -- Check spells
        if ValidTarget(target) then

            if menu.combo.useQ and spells[Q].ready then
                local castpos,  hitchance,  playerpos = VP:GetCircularCastPosition(target, spells[Q].delay, spells[Q].width, spells[Q].range, spells[Q].speed, player, false)
                if GetDistance(castpos) <= spells[Q].range then
                    if hitchance > 1 then
                        castQ(castpos.x, castpos.z)

                        -- Ultra combo since we have nearly 100% hitchance
                        if menu.combo.useR and hitchance > 3 and spells[R].ready and ultManager:CanUseUlt() then
                            castR(target)
                        end
                    end
                end
            end

            if menu.combo.useW and spells[W].ready then
                if GetDistance(target) <= spells[W].range then
                    castW()
                end
            end

            if menu.combo.useE and spells[E].ready then
                local playerpos, hitchance = VP:GetPredictedPos(target, spells[E].delay)
                -- I tweaked the range to -25 to ensure the hitchance
                if GetDistance(playerpos) <= spells[E].range - 25 then
                    if hitchance > 1 then
                        castE()
                    end
                end
            end

            if menu.combo.useR and spells[R].ready and ultManager:CanUseUlt() then
                if GetDistance(target) <= spells[R].range then
                    if ultManager:ChampHasBuff(target) then
                        castR(target)
                    end
                end
            end

        end

        -- Move to mouse
        if menu.combo.mouse and not menu.combo.orbwalk then
            moveToMouse()
        -- Orbwalk
        elseif menu.combo.mouse and menu.combo.orbwalk then
            if ValidTarget(target, player.range + player:GetDistance(player.minBBox)) then
                if GetTickCount() + GetLatency() / 2 > lastAttack + lastAttackCD then
                    attackTarget(target)
                elseif GetTickCount() + GetLatency() / 2 > lastAttack + lastWindUpTime + 20 then
                    moveToMouse()
                end
            else
                moveToMouse()
            end
        end

    end
end

function checkHarass()
    if menu.harass.active or menu.harass.toggle and not menu.combo.active then

        local harassTarget = getBestTarget(spells[Q].range)
        if harassTarget then

            if spells[Q].ready then
                local castpos,  hitchance,  playerpos = VP:GetCircularCastPosition(harassTarget, spells[Q].delay, spells[Q].width, spells[Q].range, spells[Q].speed, player, false)
                if GetDistance(castpos) <= spells[Q].range then
                    if menu.harass.headshot and hitchance > 2 or not menu.harass.headshot and hitchance > 1 then
                        castQ(castpos.x, castpos.z)
                    end
                end
            end

        end

    end
end

function getBestTarget(range)
    local damageToEnemy = 0
    local target = nil

    for _, enemy in ipairs(GetEnemyHeroes()) do
        if ValidTarget(enemy, range) then
            damageToChamp = player:CalcMagicDamage(enemy, 200)
            damage = enemy.health / damageToChamp
            if damage < damageToEnemy or not target then
                damageToEnemy = damage
                target = enemy
            end
        end
    end

    return target
end

function attackTarget(enemy)
    if not _G.evade then
        if menu.utility.packets then
            Packet('S_MOVE', {type = 3, targetNetworkId=enemy.networkID}):send()
        else
            player:Attack(enemy)
        end
    end
end

function moveToMouse()
    if not _G.evade and player:GetDistance(mousePos) > 150 then
        local moveToPos = player + (Vector(mousePos) - player):normalized() * 300
        if menu.utility.packets then
            Packet('S_MOVE', {type = 2, x = moveToPos.x, y = moveToPos.z}):send()
        else
            player:MoveTo(moveToPos.x, moveToPos.z)
        end
    end
end

function castQ(posX, posZ)
    if menu.utility.packets then
        packetCast(Q, posX, posZ)
    else
        CastSpell(Q, posX, posZ)
    end
end

function castW()
    if menu.utility.packets then
        packetCast(W)
    else
        CastSpell(W)
    end
end

function castE()
    if menu.utility.packets then
        packetCast(E)
    else
        CastSpell(E)
    end
end

function castR(target)
    if menu.utility.packets then
        packetCast(R, target)
    else
        CastSpell(R, target)
    end
end

function packetCast(id, param1, param2)
    if param1 ~= nil and param2 ~= nil then
    Packet("S_CAST", {spellId = id, toX = param1, toY = param2, fromX = param1, fromY = param2}):send()
    elseif param1 ~= nil then
    Packet("S_CAST", {spellId = id, toX = param1.x, toY = param1.z, fromX = param1.x, fromY = param1.z, targetNetworkId = param1.networkID}):send()
    else
    Packet("S_CAST", {spellId = id, toX = player.x, toY = player.z, fromX = player.x, fromY = player.z, targetNetworkId = player.networkID}):send()
    end
end


--[[
 ______   ______ _______ _  _  _ _____ __   _  ______ _______
 |     \ |_____/ |_____| |  |  |   |   | \  | |  ____ |______
 |_____/ |    \_ |     | |__|__| __|__ |  \_| |_____| ______|
]]

function OnDraw()

    -- Prechecks
    if not menu or not menu.drawing then return end

    -- Debug - buffs
    if menu.utility.debug then
        for i = 0, heroManager.iCount, 1 do

            local hero = heroManager:getHero(i)
            local count = 1
            local barPos = WorldToScreen(D3DXVECTOR3(hero.x, hero.y, hero.z))
            local posX = barPos.x - 35
            local posY = barPos.y - 30

            DrawText("Current Tick: " .. GetTickCount(), 15, posX, posY + 45, colorInfo)
            DrawText("Current Game Timer: " .. GetGameTimer(), 15, posX, posY + 60, colorInfo)

            for j = 0, hero.buffCount, 1 do
                local buff = hero:getBuff(j)

                if buff.valid then
                    DrawText("Name: " .. buff.name .. " | End Tick: " .. buff.endT, 15, posX, posY + 60 + (15 * count), colorInfo)
                    count = count + 1
                end
            end

        end
    end

    -- Draw ranges
    if menu.drawing.drawQ and spells[Q].level > 0 then
        DrawCircle2(player.x, player.y, player.z, spells[Q].range, (spells[Q].ready and colorRangeReady or colorRangeNotReady))
    end
    if menu.drawing.drawW and spells[W].level > 0 then
        DrawCircle2(player.x, player.y, player.z, spells[W].range, (spells[W].ready and colorRangeReady or colorRangeNotReady))
    end
    if menu.drawing.drawE and spells[E].level > 0 then
        DrawCircle2(player.x, player.y, player.z, spells[E].range, (spells[E].ready and colorRangeReady or colorRangeNotReady))
    end
    if menu.drawing.drawR and spells[R].level > 0 then
        DrawCircle2(player.x, player.y, player.z, spells[R].range, (spells[R].ready and colorRangeReady or colorRangeNotReady))
    end

end


--[[
 _______ _____ _______ _     _      _______ _______               ______  _______ _______ _     _ _______
    |      |   |       |____/       |       |_____| |      |      |_____] |_____| |       |____/  |______
    |    __|__ |_____  |    \_      |_____  |     | |_____ |_____ |_____] |     | |_____  |    \_ ______|
]]

function OnTick()

    -- Use auto assign stats
    if menu.utility.autoStats and player.level ~= getAssignedStats() then
        local currentAssignedStats = getAssignedStats()
        if currentAssignedStats < player.level then
            OnLevelUp(player, player.level, player.level - currentAssignedStats)
        end
    end

end

function updateVars()

    -- Spells
    for i = 0, 3 do
        spells[i].ready     = player:CanUseSpell(i) == READY
        spells[i].manaUsage = player:GetSpellData(i).mana
        spells[i].cooldown  = player:GetSpellData(i).totalCooldown
        spells[i].level     = player:GetSpellData(i).level
    end

    -- Current target
    ts:update()
    target = ts.target

    -- Current health and mana
    currentHealth = player.health
    currentMana   = player.mana

    -- Update ignite
    if ignite then
        ignite.ready = player:CanUseSpell(ignite.slot) == READY
    end

end


--[[
 _______ _______ __   _ _     _
 |  |  | |______ | \  | |     |
 |  |  | |______ |  \_| |_____|
]]

function setupMenu()

    -- Create the menu
    menu = scriptConfig("Diana Config", player.charName)

    -- Add the target selector
    menu:addTS(ts)

    -- Combo menu
    menu:addSubMenu("Combo Settings", "combo")
        menu.combo:addParam("active",  "Use combo",          SCRIPT_PARAM_ONKEYDOWN, false, 32)
        menu.combo:addParam("sep",     "",                   SCRIPT_PARAM_INFO,      "")
        menu.combo:addParam("items",   "Use items in combo", SCRIPT_PARAM_ONOFF,     true)
        menu.combo:addParam("sep",     "",                   SCRIPT_PARAM_INFO,      "")
        menu.combo:addParam("mouse",   "Move to mouse",      SCRIPT_PARAM_ONOFF,     true)
        menu.combo:addParam("orbwalk", "Orbwalk",            SCRIPT_PARAM_ONOFF,     true)
        menu.combo:addParam("sep",     "",                   SCRIPT_PARAM_INFO,      "")
        menu.combo:addParam("useQ",    "Use Q",              SCRIPT_PARAM_ONOFF,     true)
        menu.combo:addParam("useW",    "Use W",              SCRIPT_PARAM_ONOFF,     true)
        menu.combo:addParam("useE",    "Use E",              SCRIPT_PARAM_ONOFF,     true)
        menu.combo:addParam("useR",    "Use R",              SCRIPT_PARAM_ONOFF,     true)

    -- Harass menu
    menu:addSubMenu("Harass Settings", "harass")
        menu.harass:addParam("active",   "Use harass",                      SCRIPT_PARAM_ONKEYDOWN,   false, string.byte("C"))
        menu.harass:addParam("toggle",   "Toggle harass",                   SCRIPT_PARAM_ONKEYTOGGLE, false, string.byte("V"))
        menu.harass:addParam("headshot", "Only harass with high hitchance", SCRIPT_PARAM_ONOFF,       true)

    -- Item menu
    menu:addSubMenu("Item Settings", "items")
        menu.items:addParam("dfg", "Use DFG",       SCRIPT_PARAM_ONOFF, true)
        menu.items:addParam("all", "Use all items", SCRIPT_PARAM_ONOFF, true)

    -- KillSteal menu
    menu:addSubMenu("KillSteal Settings", "ks")
        menu.ks:addParam("active", "Use KillSteal", SCRIPT_PARAM_ONOFF, true)
        menu.ks:addParam("ignite", "Use Ignite",    SCRIPT_PARAM_ONOFF, true)
        menu.ks:addParam("items",  "Use Items",     SCRIPT_PARAM_ONOFF, true)

    -- Drawing menu
    menu:addSubMenu("Drawing Settings", "drawing")
        menu.drawing:addParam("lagfree", "Use lagfree circles", SCRIPT_PARAM_ONOFF, true)
        menu.drawing:addParam("drawQ",   "Draw Q Range",        SCRIPT_PARAM_ONOFF, true)
        menu.drawing:addParam("drawW",   "Draw W Range",        SCRIPT_PARAM_ONOFF, false)
        menu.drawing:addParam("drawE",   "Draw E Range",        SCRIPT_PARAM_ONOFF, false)
        menu.drawing:addParam("drawR",   "Draw R Range",        SCRIPT_PARAM_ONOFF, false)

    -- Utility menu
    menu:addSubMenu("Utility Settings", "utility")
        menu.utility:addParam("autoStats", "Automatically assign stat points",   SCRIPT_PARAM_ONOFF, false)
        menu.utility:addParam("packets",   "Use packets (spells, move, attack)", SCRIPT_PARAM_ONOFF, true)
        menu.utility:addParam("debug",     "Draw debug stuff",                   SCRIPT_PARAM_ONOFF, false)

    -- Add version at last entry
    menu:addParam("version", "Installed version:", SCRIPT_PARAM_INFO, version)


    -- Options to show permanently
    menu.combo:permaShow("active")
    menu.harass:permaShow("active")
    menu.harass:permaShow("toggle")

end


--[[
  _____  _______ _     _ _______  ______      _______ _______ _     _ _______ _______
 |     |    |    |_____| |______ |_____/      |______    |    |     | |______ |______
 |_____|    |    |     | |______ |    \_      ______|    |    |_____| |       |      

]]

function setupVars()

    -- Setup VPrediction
    VP = VPrediction()

    -- Setup UltultManager
    ultManager = UltManager()

    -- Initialize TargetSelector
    ts = TargetSelector(TARGET_NEAR_MOUSE, 600, true)
    ts.name = player.charName

end

function getAssignedStats()
    return spells[Q].level + spells[W].level + spells[E].level + spells[R].level
end

function DrawCircleNextLvl(x, y, z, radius, width, color, chordlength)
    radius = radius or 300
    quality = math.max(8,math.floor(180/math.deg((math.asin((chordlength/(2*radius)))))))
    quality = 2 * math.pi / quality
    radius = radius*.92
    local points = {}
    for theta = 0, 2 * math.pi + quality, quality do
        local c = WorldToScreen(D3DXVECTOR3(x + radius * math.cos(theta), y, z - radius * math.sin(theta)))
        points[#points + 1] = D3DXVECTOR2(c.x, c.y)
    end
    DrawLines2(points, width or 1, color or 4294967295)
end

function DrawCircle2(x, y, z, radius, color)
    if not menu.drawing.lagfree then
        return DrawCircle(x, y, z, radius, color)
    end
    local vPos1 = Vector(x, y, z)
    local vPos2 = Vector(cameraPos.x, cameraPos.y, cameraPos.z)
    local tPos = vPos1 - (vPos1 - vPos2):normalized() * radius
    local sPos = WorldToScreen(D3DXVECTOR3(tPos.x, tPos.y, tPos.z))
    if OnScreen({ x = sPos.x, y = sPos.y }, { x = sPos.x, y = sPos.y })  then
        DrawCircleNextLvl(x, y, z, radius, 1, color, 75)    
    end
end

class 'Items'
--{
    function Items:__init()

        self.TIAMAT = nil
        self.HYDRA  = nil
        self.DFG    = nil

        AddTickCallback(function() self:OnTick() end)

        return self

    end

    function Items:OnTick()

        self.TIAMAT = GetInventorySlotItem(3128)
        self.HYDRA  = GetInventorySlotItem(3128)
        self.DFG    = GetInventorySlotItem(3128)

    end

    function Items:GetComboItems()

        return { self.TIAMAT,
                 self.HYDRA,
                 self.DFG }

    end

    function Items:GetSpamItems()

        return { self.DFG }

    end
--}

class 'UltManager'
--{
    function UltManager:__init()

        self.buffTable = {}
        self.lastUltTime = 0

        -- Setup buff table
        for _,enemy in ipairs(GetEnemyHeroes()) do
            self.buffTable[enemy.networkID] = { hasBuff = false, endT = 0}
        end

        AdvancedCallback:bind("OnGainBuff", function(unit, buff) self:OnGainBuff(unit, buff) end)
        AdvancedCallback:bind("OnLoseBuff", function(unit, buff) self:OnLoseBuff(unit, buff) end)
        AddProcessSpellCallback(function(unit, spell) self:OnProcessSpell(unit, spell) end)

        return self

    end

    function UltManager:OnGainBuff(unit, buff)

        -- Prevent errors
        if not unit or not unit.valid or not unit.type == "obj_AI_Hero" or unit.team == player.team or not buff or not buff.name then return end

        -- Apply value
        if buff.name == spells[Q].buffName then
            if self.buffTable[unit.networkID] then
                self.buffTable[unit.networkID].hasBuff = true
                self.buffTable[unit.networkID].endT = buff.endT
            end 
        end

    end

    function UltManager:OnLoseBuff(unit, buff)

        -- Prevent errors
        if not unit or not unit.valid or not unit.type == "obj_AI_Hero" or unit.team == player.team or not buff or not buff.name then return end

        -- Apply value
        if buff.name == spells[Q].buffName then
            if self.buffTable[unit.networkID] then
                self.buffTable[unit.networkID].hasBuff = false
            end 
        end

    end

    function UltManager:OnProcessSpell(unit, spell)

        -- Prevent errors
        if not unit or not unit.valid or not unit.isMe or not spell or not spell.name then return end

        -- Check if it's our ult
        if spell.name == spells[R].spellName then
            self:ResetBuffs()
            self.lastUltTime = GetGameTimer()
        end

    end

    function UltManager:ResetBuffs()
        -- Clear everyones moonlight buff
        for _,enemy in ipairs(self.buffTable) do
            self.buffTable[enemy].hasBuff = false
        end
    end

    function UltManager:ChampHasBuff(champ)

        -- Prevent errors
        if not champ or not champ.valid or not champ.type == "obj_AI_Hero" or champ.team == player.team then return end

        return self.buffTable[champ.networkID] and self.buffTable[champ.networkID].hasBuff

    end

    function UltManager:CanUseUlt()

        return GetGameTimer() >= self.lastUltTime + spells[R].delay 

    end
--}

--UPDATEURL=
--HASH=70616642DD9219B1BC6E4A1834D10934
