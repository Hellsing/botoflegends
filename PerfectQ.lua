-- Change autoUpdate to false if you wish to not receive auto updates.
local version    = 1.105
local autoUpdate = true

--[[
    _  _ ____ ____ _  _ ____ 
    |\ | |__| [__  |  | [__  
    | \| |  | ___] |__| ___] 

    PerfectQ

    Requirements:
        - VIP Account
        - JungleLib (will be auto aquired)

    Features:
        - Very very fast combo (Q -> AA) thanks to packets
        - Marking minions which are ready to kill with Q
        - Won't hit minions when combo active (changeable combo key in menu)
        - Smart jungle farming, you will see what I mean by this :)

    ToDo:
        - Smite + Q combo for jungler
        - Priority on big minions on lanes

    Known bugs:
        - When reloading it has no info about your current stacks, kill a minion with Q first
          to get it updated (dunno if it's fixable)

    Changelog:
        - Version 0.001 (03/09/2014):
            - Initial release
        - Version 0.002 (03/09/2014):
            - Added item damage into calculation
            - Improved damage calculation, still not perfect
            - Tweaked range
            - Added options to enable/disable drawing
        - Version 0.003 (03/10/2014):
            - Reworked menu
            - Added jungle farming
            - Fixed bug where the script was not disabled when pressing the key for it
        - Version 0.004 (03/10/2014)
            - Fixed bug where the script won't autoattack minions
        - Version 0.100 (beta) (03/11/2014)
            - Reworked jungle farming
            - Reworked damage calculation
            - First try to implement masteries into damage calculation
            - Added debug info menu
            - Cleaned up clode
        - Version 0.101 (beta) (03/11/2014)
            - Fixed drawing killable minions
            - Added more debug info
        - Version 0.102 (beta) (03/11/2014)
            - Trief do fix jungle farming again
            - Fixed drawing again
            - Even more debug info
        - Version 0.103 (beta) [03/12/2014]
            - Added damage indicators for Q damage on enemy champs
            - Improved code style
        - Version 1.104 [03/15/2014]
            - Improved jungle code
            - Made a stable version
        - Version 1.105 [03/15/2014]
            - Fixed attack speed

]]

if myHero.charName ~= "Nasus" or not VIP_USER then return end

-------------Updater-------------

local UPDATE_HOST      = "bitbucket.org"
local UPDATE_PATH      = "/Hellsing/botoflegends/raw/master/PerfectQ.lua"
local UPDATE_FILE_PATH = SCRIPT_PATH..GetCurrentEnv().FILE_NAME
local UPDATE_URL       = "https://"..UPDATE_HOST..UPDATE_PATH
local SCRIPT_NAME      = "PerfectQ"

local webResult = nil
if autoUpdate then
    DelayAction(function() GetAsyncWebResult(UPDATE_HOST, UPDATE_PATH, function(data) webResult = data end) end, 3)
    function updateScript()
        if webResult then
            local serverVersion
            local startPos, endPos, junk = nil, nil, nil
            junk, startPos = string.find(webResult, "local version    = ")
            if startPos then
                endPos, junk = string.find(webResult, "\n", startPos)
            end
            if endPos then
                serverVersion = tonumber(string.sub(webResult, startPos, endPos))
            end
            if serverVersion and serverVersion > version then
                DownloadFile(UPDATE_URL.."?nocache"..myHero.charName..os.clock(), UPDATE_FILE_PATH, function () print("<font color=\"#8080F0\">"..SCRIPT_NAME..": Successfully updated. (v"..version.." -> v"..serverVersion..")</font>") end)
            elseif not serverVersion then
                print("<font color=\"#8080F0\">"..SCRIPT_NAME..": Something went wrong! Please manually update the script!</font>")
            end
            webResult = nil
        end
    end
    AddTickCallback(updateScript)
end

----------- JungleLib -----------


------------ Globals ------------

local enemyMinions = nil
local jungleMobs   = nil
local jungleLib    = nil
local menu         = nil

local buffActive = false
local buffStacks = 0
local buffDamage = 0

local attackSpeed = 0

local lastAttack     = 0
local lastWindUpTime = 0
local lastAttackCD   = 0

local debug = {}

----------- Constants -----------

local TRUE_RANGE = 125 + player:GetDistance(player.minBBox)
local BUFF_NAME  = "NasusQ"
local DAMAGE_Q   = { 30, 50, 70, 90, 110 }

local BASE_ATTACKSPEED       = 0.638

local ITEMS = { [3057] = { name = "Sheen",            unique = "SPELLBLADE", buffName = "sheen",          buffActive = false, bonusDamage = 0, multiplier = 1 },
                [3025] = { name = "Iceborn Gauntlet", unique = "SPELLBLADE", buffName = "itemfrozenfist", buffActive = false, bonusDamage = 0, multiplier = 1.25} }


--[[
 _______  _____  ______  _______
 |       |     | |     \ |______
 |_____  |_____| |_____/ |______
]]

function OnLoad()

    -- Aquire JungleLib
    DownloadFile("https://bitbucket.org/Hellsing/botoflegends/raw/master/lib/JungleLib.lua", LIB_PATH .. "JungleLib.lua",
        function ()
            require "JungleLib"
            jungleLib = JungleLib()
        end
    )

    -- Enemy minion manager
    enemyMinions = minionManager(MINION_ENEMY, 1000, player, MINION_SORT_MAXHEALTH_DEC)

    -- Menu
    menu = scriptConfig(SCRIPT_NAME, player.charName)

    menu:addSubMenu("Masteries", "masteries")
        menu.masteries:addParam("butcher", "Butcher",      SCRIPT_PARAM_SLICE, 0, 0, 1, 0)
        menu.masteries:addParam("arcane",  "Arcane Blade", SCRIPT_PARAM_SLICE, 0, 0, 1, 0)
        menu.masteries:addParam("havoc",   "Havoc",        SCRIPT_PARAM_SLICE, 0, 0, 1, 0)

    menu:addSubMenu("Jungle Farm Settings", "jungle")
        menu.jungle:addParam("active",  "Farm jungle",             SCRIPT_PARAM_ONKEYDOWN, false, string.byte("T"))
        menu.jungle:addParam("orbwalk", "Orbwalk while farming",   SCRIPT_PARAM_ONOFF,     true)
        menu.jungle:addParam("sep",     "",                        SCRIPT_PARAM_INFO,      "")
        menu.jungle:addParam("smart",   "Smart combo (Smite + Q)", SCRIPT_PARAM_ONOFF,     true)
        menu.jungle:addParam("sep",     "",                        SCRIPT_PARAM_INFO,      "")
        menu.jungle:addParam("draw",    "Draw jungle stuff",       SCRIPT_PARAM_ONOFF,     true)

    menu:addSubMenu("Debug Information", "debug")
        menu.debug:addParam("lastdmg",     "Last Q damage calculated: ",  SCRIPT_PARAM_INFO, 0);
        menu.debug:addParam("sep",         "",                            SCRIPT_PARAM_INFO, "");
        menu.debug:addParam("jungleCount", "Jungle minions around you: ", SCRIPT_PARAM_INFO, 0)
        menu.debug:addParam("sep",         "",                            SCRIPT_PARAM_INFO, "");
        menu.debug:addParam("attackSpeed", "Attack Speed: ",              SCRIPT_PARAM_INFO, 0);
        menu.debug:addParam("cooldownQ",   "Cooldown for Q: ",            SCRIPT_PARAM_INFO, 0)
        menu.debug:addParam("hitsWhileCD", "AA hits while Q cooldown: ",  SCRIPT_PARAM_INFO, 0);

    menu:addParam("sep",         "",                                 SCRIPT_PARAM_INFO,        "")
    menu:addParam("sep",         "",                                 SCRIPT_PARAM_INFO,        "")
    menu:addParam("combo",       "Disable script (key down)",        SCRIPT_PARAM_ONKEYDOWN,   false, 32)
    menu:addParam("disabled",    "Disable script",                   SCRIPT_PARAM_ONKEYTOGGLE, false, string.byte("N"))
    menu:addParam("sep",         "",                                 SCRIPT_PARAM_INFO,        "")
    menu:addParam("drawRange",   "Draw auto-attack range",           SCRIPT_PARAM_ONOFF,       true)
    menu:addParam("drawIndic",   "Draw damage indicator on enemies", SCRIPT_PARAM_ONOFF,       true)
    menu:addParam("markMinions", "Mark killable minions",            SCRIPT_PARAM_ONOFF,       true)
    menu:addParam("sep",         "",                                 SCRIPT_PARAM_INFO,        "")
    menu:addParam("version",     "Installed Version:",               SCRIPT_PARAM_INFO,        version)

end

function OnTick()

    -- Update debug menu
    menu.debug.lastdmg     = debug["LastDamage"]
    if jungleLib then menu.debug.jungleCount = jungleLib:MobCount(true, TRUE_RANGE * 2) end
    menu.debug.attackSpeed = attackSpeed
    menu.debug.hitsWhileCD = debug["HitsWhileCooldown"]
    menu.debug.cooldownQ   = debug["CooldownQ"]

    -- Update minion managers
    enemyMinions:update()

    -- Item checks
    local itemDamage = 0
    for itemID, entry in pairs(ITEMS) do
        if GetInventorySlotItem(itemID) and (player:CanUseSpell(GetInventorySlotItem(itemID)) == READY or ITEMS[itemID].buffActive) then

            -- Update bunus damage
            ITEMS[itemID].bonusDamage = player.damage * ITEMS[itemID].multiplier

            if ITEMS[itemID].bonusDamage > itemDamage then
                itemDamage = ITEMS[itemID].bonusDamage
            end

        end
    end

    -- Update buff damage
    buffDamage = buffStacks + itemDamage + (player:GetSpellData(_Q).level > 0 and DAMAGE_Q[player:GetSpellData(_Q).level] or 0)

    -- Update attackspeed
    attackSpeed = player.attackSpeed * BASE_ATTACKSPEED

    -- Prechecks
    if menu.combo or menu.disabled then return end

    -- Hit em, but hit em hard!
    timeForPerfectQ()

    -- Prechecks for jungling
    if not menu.jungle.active then return end

    -- Alright, let's doooh eeeht!
    clearDemJungle()

end


--[[
        _______ __   _ _______      _______ _______  ______ _______ _____ __   _  ______
 |      |_____| | \  | |______      |______ |_____| |_____/ |  |  |   |   | \  | |  ____
 |_____ |     | |  \_| |______      |       |     | |    \_ |  |  | __|__ |  \_| |_____|
 ]]

function timeForPerfectQ()

    -- Lane minions
    for _, minion in pairs(enemyMinions.objects) do

        if ValidTarget(minion, TRUE_RANGE) then

            -- Calculate damage
            local damage = calculateDamage(minion, buffDamage)

            if minion.health <= damage then
                -- Ready Q
                if not buffActive and player:CanUseSpell(_Q) == READY then
                    packetCast(_Q)
                    packetAttack(minion)
                    debug["LastDamage"] = damage
                    break
                end

                if buffActive then
                    packetAttack(minion)
                    debug["LastDamage"] = damage
                    break
                end
            end
        end
    end

end


--[[
 _____ _     _ __   _  ______        _______      _______ _______ _     _ _______ _______
   |   |     | | \  | |  ____ |      |______      |______    |    |     | |______ |______
 __|   |_____| |  \_| |_____| |_____ |______      ______|    |    |_____| |       |      
 ]]

function clearDemJungle()

    local cooldownQ         = player:GetSpellData(_Q).totalCooldown
    debug["CooldownQ"]      = cooldownQ

    local hitsWhileCooldown    = math.floor(cooldownQ / (1 / attackSpeed))
    debug["HitsWhileCooldown"] = hitsWhileCooldown

    for _, mob in pairs(jungleLib:GetJungleMobs(true, TRUE_RANGE * 2)) do

        local damageAA = calculateDamage(mob)
        local damageQ  = calculateDamage(mob, buffDamage)

        local damageWhileCooldown = hitsWhileCooldown * damageAA

        if menu.jungle.orbwalk and ValidTarget(mob, TRUE_RANGE) or not menu.jungle.orbwalk and ValidTarget(mob, TRUE_RANGE * 2) then
            if (damageQ >= mob.health or mob.health > damageWhileCooldown + damageQ) and (player:CanUseSpell(_Q) == READY or buffActive) then
                if not buffActive then packetCast(_Q) end
                packetAttack(mob)
                debug["LastDamage"] = damageQ
                return
            elseif GetTickCount() + GetLatency() / 2 > lastAttack + lastAttackCD then
                if mob.health > damageAA then
                    packetAttack(mob)
                    debug["LastDamage"] = damageQ
                    return
                end
            else
                break
            end
        end

    end

    -- Jungle orbwalker
    if menu.jungle.orbwalk and GetTickCount() + GetLatency() / 2 > lastAttack + lastWindUpTime + 20 then
        moveToMouse()
    end

end


--[[
  ______ _______ __   _ _______  ______ _______             _______ _______               ______  _______ _______ _     _ _______
 |  ____ |______ | \  | |______ |_____/ |_____| |           |       |_____| |      |      |_____] |_____| |       |____/  |______
 |_____| |______ |  \_| |______ |    \_ |     | |_____      |_____  |     | |_____ |_____ |_____] |     | |_____  |    \_ ______|
]]

function OnProcessSpell(unit, spell)

    -- Prevent errors
    if not unit or not unit.valid or not unit.isMe then return end
  
    if spell.name:lower():find("attack") then
        lastAttack = GetTickCount() - GetLatency() / 2
        lastWindUpTime = spell.windUpTime * 1000
        lastAttackCD = spell.animationTime * 1000
    end

end

function OnRecvPacket(p)

    if p.header == 0xFE and p.size == 0xC then
        p.pos = 1
        local networkID = p:DecodeF()

        if(networkID == player.networkID) then
            p.pos = 8
            buffStacks = p:Decode4()
        end
    end

end

function OnGainBuff(unit, buff)

    -- Validate unit
    if not unit or not unit.isMe then return end

    -- Validate buff
    if not buff or not buff.name then return end


    -- Check buff
    if buff.name == BUFF_NAME then
        buffActive = true
    end

    -- Check item buffs
    for itemID, entry in pairs(ITEMS) do
        if GetInventorySlotItem(itemID) and ITEMS[itemID].buffName == buff.name then
            ITEMS[itemID].buffActive = true
            break
        end
    end

end

function OnLoseBuff(unit, buff)

    -- Validate unit
    if not unit or not unit.isMe then return end

    -- Validate buff
    if not buff or not buff.name then return end


    -- Check buff
    if buff.name == BUFF_NAME then
        buffActive = false
    end

    -- Check item buffs
    for itemID, entry in pairs(ITEMS) do
        if GetInventorySlotItem(itemID) and ITEMS[itemID].buffName == buff.name then
            ITEMS[itemID].buffActive = false
            break
        end
    end

end


--[[
 ______   ______ _______ _  _  _ _____ __   _  ______ _______
 |     \ |_____/ |_____| |  |  |   |   | \  | |  ____ |______
 |_____/ |    \_ |     | |__|__| __|__ |  \_| |_____| ______|
]]

function OnDraw()

    -- Draw our range
    if menu.drawRange then DrawCircle3D(player.x, player.y, player.z, TRUE_RANGE, 1, ARGB(255, 255, 50,  0), 10) end

    -- Draw minions which are ready to get killed
    if menu.markMinions then
        for _, minion in pairs(enemyMinions.objects) do

            if calculateDamage(minion, buffDamage) >= minion.health then
                DrawCircle3D(minion.x, minion.y, minion.z, Vector(minion.x, minion.y, minion.z):dist(Vector(minion.minBBox.x, minion.minBBox.y, minion.minBBox.z)), 1, ARGB(255, 255, 50,  0), 10)
            end

        end
    end

    -- Draw damage indicators
    if menu.drawIndic then
        for _, enemy in ipairs(GetEnemyHeroes()) do
            if (ValidTarget(enemy)) then
                DrawIndicator(enemy, enemy.health - math.floor(calculateDamage(enemy, buffDamage)))
            end
        end
    end

    -- Draw jungle stuff
    if menu.jungle.draw and jungleLib then
        local priorityMob = jungleLib:GetJungleMobs(true, TRUE_RANGE * 2)[1]

        if priorityMob then DrawCircle3D(priorityMob.x, priorityMob.y, priorityMob.z, Vector(priorityMob.x, priorityMob.y, priorityMob.z):dist(Vector(priorityMob.minBBox.x, priorityMob.minBBox.y, priorityMob.minBBox.z)), 1, ARGB(255, 255, 50,  0), 10) end
    end

end

-- Credits to Zikkah for this, just added validations
function GetHPBarPos(enemy)

    enemy.barData = GetEnemyBarData()
    local barPos = GetUnitHPBarPos(enemy)
    local barPosOffset = GetUnitHPBarOffset(enemy)

    -- Validation
    if enemy.barData == nil or barPos == nil or barPosOffset == nil then return end

    local barOffset = { x = enemy.barData.PercentageOffset.x, y = enemy.barData.PercentageOffset.y }
    local barPosPercentageOffset = { x = enemy.barData.PercentageOffset.x, y = enemy.barData.PercentageOffset.y }
    local BarPosOffsetX = 171
    local BarPosOffsetY = 46
    local CorrectionY =  0
    local StartHpPos = 31
    barPos.x = barPos.x + (barPosOffset.x - 0.5 + barPosPercentageOffset.x) * BarPosOffsetX + StartHpPos
    barPos.y = barPos.y + (barPosOffset.y - 0.5 + barPosPercentageOffset.y) * BarPosOffsetY + CorrectionY 
                        
    local StartPos = Vector(barPos.x , barPos.y, 0)
    local EndPos =  Vector(barPos.x + 108 , barPos.y , 0)

    return Vector(StartPos.x, StartPos.y, 0), Vector(EndPos.x, EndPos.y, 0)

end

-- Credits to honda7 for this, just added validations and different colors
function DrawIndicator(unit, health)

    local SPos, EPos = GetHPBarPos(unit)

    -- Validation
    if SPos == nil or EPos == nil then return end

    local barlenght = EPos.x - SPos.x
    local Position = SPos.x + (health / unit.maxHealth) * barlenght
    if Position < SPos.x then
        Position = SPos.x
    end
    DrawText("|", 13, Position, SPos.y+10, (health > 0 and ARGB(255, 0, 255, 0) or ARGB(255, 255, 0, 0)))

end


--[[
  _____  _______ _     _ _______  ______      _______ _______ _     _ _______ _______
 |     |    |    |_____| |______ |_____/      |______    |    |     | |______ |______
 |_____|    |    |     | |______ |    \_      ______|    |    |_____| |       |      

]]

function calculateDamage(target, bonusDamage)
    -- read initial armor and damage values
    local armorPenPercent = player.armorPenPercent
    local armorPen = player.armorPen
    local totalDamage = (player.totalDamage + (bonusDamage or 0) + (menu.masteries.butcher == 1 and 2 or 0)) * (menu.masteries.havoc == 1 and 1.03 or 1)
    local damageMultiplier = 1

    -- turrets ignore armor penetration and critical attacks
    if target.type == "obj_AI_Turret" then
        armorPenPercent = 1
        armorPen = 0
    end

    -- calculate initial damage multiplier for negative and positive armor
    local targetArmor = (target.armor * armorPenPercent) - armorPen
    if targetArmor >= 0 then 
        damageMultiplier = 100 / (100 + targetArmor) * damageMultiplier
    end

    -- use ability power or ad based damage on turrets
    if target.type == "obj_AI_Turret" then
        damageMultiplier = 0.95 * damageMultiplier
        totalDamage = math.max(player.totalDamage, player.damage + 0.4 * player.ap)
    end

    -- calculate damage dealt including masteries
    return damageMultiplier * totalDamage + (menu.masteries.arcane == 1 and (player:CalcMagicDamage(target, 0.05 * player.ap)) or 0) 
end

function isInside(target, distance, source)

    source = source or player
    return GetdistanceSqr(target, source) <= distance ^ 2

end

function moveToMouse()
    if not _G.evade then
        local moveToPos = player + (Vector(mousePos) - player):normalized() * 300
        Packet('S_MOVE', {type = 2, x = moveToPos.x, y = moveToPos.z}):send()
    end
end

function packetCast(id, param1, param2)
    if param1 ~= nil and param2 ~= nil then
    Packet("S_CAST", {spellId = id, toX = param1, toY = param2, fromX = param1, fromY = param2}):send()
    elseif param1 ~= nil then
    Packet("S_CAST", {spellId = id, toX = param1.x, toY = param1.z, fromX = param1.x, fromY = param1.z, targetNetworkId = param1.networkID}):send()
    else
    Packet("S_CAST", {spellId = id, toX = player.x, toY = player.z, fromX = player.x, fromY = player.z, targetNetworkId = player.networkID}):send()
    end
end

function packetAttack(enemy)
    Packet('S_MOVE', {type = 3, targetNetworkId=enemy.networkID}):send()
end
