
 ______         _     _ _______
 |_____] |      |     | |______
 |_____] |_____ |_____| |______
                               


BIG WRAITH:

    GreatWraith13.1.1


BLUE BUFF:

    YoungLizard1.1.3
    YoungLizard1.1.2
    AncientGolem1.1.1


WOLVES:

    Wolf2.1.3
    Wolf2.1.2
    GiantWolf2.1.1


SMALL WRAITHS:

    LesserWraith3.1.4
    LesserWraith3.1.3
    LesserWraith3.1.2
    Wraith3.1.1


RED BUFF:

    YoungLizard4.1.3
    YoungLizard4.1.2
    LizardElder4.1.1


GOLEMS:

    Golem5.1.2
    SmallGolem5.1.1


        _______ _     _ _______
 |      |_____| |____/  |______
 |_____ |     | |    \_ |______



BARON:

    Worm12.1.1


DRAGON:

    Dragon6.1.1


  _____  _     _  ______  _____         _______
 |_____] |     | |_____/ |_____] |      |______
 |       |_____| |    \_ |       |_____ |______
                                               


GOLEMS:

    Golem11.1.2
    SmallGolem11.1.1


RED BUFF:

    LizardElder10.1.1
    YoungLizard10.1.2
    YoungLizard10.1.3


SMALL WRAITHS:

    LesserWraith9.1.4
    LesserWraith9.1.3
    LesserWraith9.1.2
    Wraith9.1.1


WOLVES:

    Wolf8.1.3
    Wolf8.1.2
    GiantWolf8.1.1


BLUE BUFF:

    YoungLizard7.1.3
    YoungLizard7.1.2
    AncientGolem7.1.1


BIG WRAITH:

    GreatWraith14.1.1
